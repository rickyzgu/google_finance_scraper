# -*- encoding: utf-8 -*-
require File.expand_path('../lib/google_finance_scraper/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Huned Botee"]
  gem.email         = ["huned@734m.com"]
  gem.summary       = %q{A scraper for Google Finance in ruby.}
  gem.description   = %q{Scrape most active stocks, detailed stock quotes, historical prices, and options chain prices from Google Finance with ruby.}
  gem.homepage      = "http://github.com/huned/google_finance_scraper"

  gem.files         = `git ls-files`.split($\)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = "google_finance_scraper"
  gem.require_paths = ["lib"]
  gem.version       = GoogleFinance::Scraper::VERSION

  gem.add_dependency 'nokogiri'

  #gem.add_development_dependency 'byebug'
  gem.add_development_dependency 'rspec'
end
