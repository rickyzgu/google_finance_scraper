require "google_finance_scraper/version"
require 'csv'
require 'net/http'
require 'nokogiri'

module GoogleFinance
  module Scraper
    class Company
      attr_reader :symbol
      attr_accessor :getter

      def initialize symbol, options = {}
        @symbol = symbol
        @getter = options[:getter]
      end

      def details
        raise NotImplementedError, 'TODO'
      end

      def historical_prices from = nil, to = nil
        to ||= Date.today
        from ||= to - 731 # 2 years (+ 1 day in case of leap year)
        url = historical_prices_url from, to

        CSV.parse(get(url))[1..-1].map.with_index do |row, i|
          next if i == 0 # skip header row
          date, open, high, low, close, volume = row
          { open: open.to_f, high: high.to_f, low: low.to_f,
            close: close.to_f, volume: volume.to_f, adj_close: nil,
            date: Date.strptime(date, '%e-%b-%y').to_time.utc }
        end.compact.sort_by {|h| h[:date] }
      end

      def options_chain
        raise NotImplementedError, 'TODO'
      end

      private

      def get(url)
        response = (getter || Net::HTTP).get_response(URI.parse(url))
        response_code = response.code.to_i
        case response_code
        when 200
          response.body
        when 400, 404
          raise Net::HTTPError, "#{url} responded with #{response_code}:\n#{response.body}"
        else
          raise Net::HTTPError, "#{url} responded with #{response_code}:\n#{response.body}"
        end
      end

      def historical_prices_url from, to
        'http://www.google.com/finance/historical?q=%s&startdate=%s&enddate=%s&output=csv' % [
          symbol, 
          from.strftime('%b+%d,+%Y'),
          to.strftime('%b+%d,+%Y')
        ]
      end
    end
  end
end
